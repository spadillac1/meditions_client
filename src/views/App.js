import React, { lazy } from 'react';
import { Grid, Header, Tab } from 'semantic-ui-react';

import './App.scss';

const Users = lazy(() => import('../components/Users/Users'));
const Meditions = lazy(() => import('../components/Meditions/Meditions'));

const App = () => {
  const panes = [
    {
      menuItem: 'Mediciones',
      render: () => (
        <Tab.Pane inverted attached="top">
          <Meditions />
        </Tab.Pane>
      ),
    },
    {
      menuItem: 'Usuarios',
      render: () => (
        <Tab.Pane inverted attached="top">
          <Users />
        </Tab.Pane>
      ),
    },
  ];

  return (
    <Grid className="app">
      <Grid.Column className="app__column">
        <Header inverted as="h1">
          Control de Mediciones
        </Header>
        <Tab
          menu={{ secondary: true, inverted: true, pointing: true }}
          panes={panes}
        />
      </Grid.Column>
    </Grid>
  );
};

export default App;
