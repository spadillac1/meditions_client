import axios from 'axios';

import { users, meditions } from './enviroment';

export const getUsers = () => axios.get(users).then((res) => res.data);
export const deleteUser = (id) => axios.delete(`${users}/${id}`);
export const postUser = (data) => axios.post(users, data);

export const getMeditions = () => axios.get(meditions).then((res) => res.data);
export const deleteMedition = (id) => axios.delete(`${meditions}/${id}`);
