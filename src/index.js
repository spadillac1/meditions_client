import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';

import 'semantic-ui-css/semantic.min.css';
import { Loader } from 'semantic-ui-react';

import './index.scss';

const App = lazy(() => import('./views/App'));

ReactDOM.render(
  <Suspense
    fallback={
      <Loader inverted active>
        Loading...
      </Loader>
    }
  >
    <App />
  </Suspense>,
  document.getElementById('root')
);
