import React, { useState, useEffect } from 'react';
import { Button, Card, Icon, Label } from 'semantic-ui-react';

import { deleteMedition, getMeditions } from '../../helpers/apiRequest';
import MeditionsModal from './MeditionsModal';

const MeditionsCard = ({ meditionData, className, onAction }) => {
  const [medition, setMedition] = useState(meditionData);

  useEffect(() => {
    if (meditionData !== undefined) {
      setMedition(meditionData);
    }
  }, [meditionData]);

  const handleDelete = (id) => {
    console.log(id);
    alert('delete');
    // deleteMedition(id)
    // .then(() => {
    // alert('User was removed');
    // getMeditions()
    // .then((data) => onAction(data))
    // .catch(console.error);
    // })
    /* .catch(console.error); */
  };

  /*   const updateUserTrigger = ( */
  // <Button color="blue" icon>
  // <Icon name="edit" />
  // </Button>
  /* ); */

  // <div style={{display: 'flex'}}>
  return (
    <Card>
      <Card.Content>
        <Icon
          onClick={() => handleDelete(medition._id)}
          style={{ position: 'absolute', right: '0' }}
          color="red"
          inverted
          name="window close"
          size="big"
        ></Icon>
        <Card.Header>{medition.name}</Card.Header>
        <Card.Description>
          <Label.Group size="tiny">
            <Label>
              <strong>Coordenadas:</strong>
              <Label.Detail>{medition.coordenada1}</Label.Detail>
              <Label.Detail>{medition.coordenada2}</Label.Detail>
              <Label.Detail>{medition.coordenada3}</Label.Detail>
              <Label.Detail>{medition.coordenada4}</Label.Detail>
            </Label>
          </Label.Group>

          <Label.Group size="small" color="blue">
            <Label>
              <strong>Locacion:</strong>
              <Label.Detail>{medition.locacion}</Label.Detail>
            </Label>
            <Label>
              <strong>Area:</strong>
              <Label.Detail>{medition.area}</Label.Detail>
            </Label>
            <Label>
              <strong>mt2:</strong>
              <Label.Detail>{medition.mt2}</Label.Detail>
            </Label>
          </Label.Group>
        </Card.Description>
      </Card.Content>
    </Card>
  );
};

/* <Card.Content extra> */
// <Button color="red" icon onClick={() => handleDelete(medition._id)}>
// <Icon name="trash" />
// </Button>
/* </Card.Content> */

//&ensp;
/*         <MeditionsModal */
// data={medition}
// trigger={updateUserTrigger}
// onClick={onAction}
/* /> */

export default MeditionsCard;
