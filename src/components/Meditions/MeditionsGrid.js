import React, { useState, useEffect } from 'react';

import { Card } from 'semantic-ui-react';

import MeditionsCard from './MeditionsCard';

const Meditions = ({ results, className, onAction }) => {
  const [receivedResults, setReceivedResults] = useState([]);

  useEffect(() => {
    if (results !== undefined) {
      setReceivedResults(results);
    }
  }, [results]);

  return (
    <Card.Group className={className} itemsPerRow={4} doubling>
      {receivedResults.map((result) => (
        <MeditionsCard key={result.name} meditionData={result} onAction={onAction} />
      ))}
    </Card.Group>
  );
};

export default Meditions;

