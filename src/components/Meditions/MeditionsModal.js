import React, { useState } from 'react';
import { Button, Form, Header, Modal, Icon } from 'semantic-ui-react';

import { useForm } from '../../hooks/useForm';
import { meditionsForm } from '../../helpers/models';
/* import { */
// getUsers,
// postUsers,
// putUsers,
/* } from '../../helpers/api'; */

const MeditionsModal = ({ data, trigger, onClick }) => {
  const [formState, handleInputChange] = useForm(data || meditionsForm);
  const [open, setOpen] = useState(false);

  const { userId, nombre, coordenada1, coordenada2 } = formState;

  const handleSubmit = () => {
    setOpen(false);
    // if (!data) {
    /*       postUsers(formState) */
    // .then(() => {
    // getUsers(onClick)
    // .then((users) => {
    // alert('User was created');
    // onClick(users);
    // setOpen(false);
    // })
    // .catch(console.error);
    // })
    /* .catch(console.error); */
    // } else {
    /*       putUsers(formState._id, formState) */
    // .then(() => {
    // getUsers(onClick)
    // .then((users) => {
    // alert('User was updated');
    // onClick(users);
    // setOpen(false);
    // })
    // .catch(console.error);
    // setOpen(false);
    // })
    // .catch(console.error);
    /* } */
  };

  return (
    <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size="small"
      trigger={trigger}
    >
      <Header icon>
        <Icon name="resize horizontal" />
        Agregar Medición
      </Header>
      <Modal.Content>
        <Form>
          <Form.Group widths="equal">
            <select>
              {' '}
              <option value="Test"> Seleccione Usuario </option>{' '}
            </select>
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Nombre"
              name="nombre"
              placeholder="nombre"
              required
              value={nombre}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="number"
              label="Coordenada 1"
              name="coordenada1"
              placeholder="coordenada1"
              required
              value={coordenada1}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="number"
              label="Coordenada 2"
              name="coordenada2"
              placeholder="coordenada2"
              required
              value={coordenada2}
            />
          </Form.Group>
        </Form>
      </Modal.Content>

      <Modal.Actions>
        <Button color="red" inverted onClick={() => setOpen(false)}>
          <Icon name="remove" /> Cancel
        </Button>
        <Button color="green" inverted onClick={handleSubmit}>
          <Icon name="checkmark" /> {data ? 'Update' : 'Registry'}
        </Button>
      </Modal.Actions>
    </Modal>
  );
};
export default MeditionsModal;
