import React, { useEffect, lazy, useState } from 'react';
import { Button } from 'semantic-ui-react';

import { getMeditions } from '../../helpers/apiRequest';

import './Meditions.scss';

const MeditionsModal = lazy(() => import('./MeditionsModal'));
const MeditionsGrid = lazy(() => import('./MeditionsGrid'));

const Meditions = () => {
  const [meditions, setMeditions] = useState([]);

  useEffect(() => {
    getMeditions()
      .then((data) => {console.log(data); setMeditions(data)})
      .catch((error) => console.error(error.message));
  }, []);

  const addUserTrigger = (
    <Button inverted fluid>
      Agregar nueva Medición
    </Button>
  );

  return (
    <>
      <MeditionsGrid
        className="results"
        results={meditions}
        onAction={setMeditions}
      />
    </>
  );
};

//<MeditionsModal trigger={addUserTrigger} onClick={setMeditions} />
export default Meditions;
