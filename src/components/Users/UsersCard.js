import React, { useState, useEffect } from 'react';
import { Button, Card, Icon, Label } from 'semantic-ui-react';

import { deleteUser, getUsers } from '../../helpers/apiRequest';
// import UsersModal from './UsersModal';

const UsersCard = ({ userData, className, onAction }) => {
  const [user, setUser] = useState({});

  useEffect(() => {
    if (userData !== undefined) {
      setUser(userData);
      console.log(userData);
    }
  }, [userData]);

  const handleDelete = (id) => {
    console.log(id);
    /*     alert('delete'); */
    // deleteUser(id)
    // .then(() => {
    // alert('User was removed');
    // getUsers()
    // .then((data) => onAction(data))
    // .catch(console.error);
    // })
    /* .catch(console.error); */
  };

  /*   const updateUserTrigger = ( */
  // <Button color="blue" icon>
  // <Icon name="edit" />
  // </Button>
  /* ); */

  // <div style={{display: 'flex'}}>
  return (
    <Card>
      <Card.Content>
        <Icon
          onClick={() => handleDelete(user._id)}
          style={{ position: 'absolute', right: '0' }}
          color="red"
          inverted
          name="window close"
          size="big"
        ></Icon>
        <Card.Header>{user.nombre}{user.apellido} </Card.Header>
        <Card.Description>
          <Label.Group size="tiny">
            <Label>
              <strong>Datos:</strong>
              <Label.Detail>{user.pais}</Label.Detail>
              <Label.Detail>{user.telefono}</Label.Detail>
            </Label>
          </Label.Group>
        </Card.Description>
      </Card.Content>
    </Card>
  );
};

/* <Card.Content extra> */
// <Button color="red" icon onClick={() => handleDelete(user._id)}>
// <Icon name="trash" />
// </Button>
/* </Card.Content> */

//&ensp;
/*         <UsersModal */
// data={user}
// trigger={updateUserTrigger}
// onClick={onAction}
/* /> */

export default UsersCard;
