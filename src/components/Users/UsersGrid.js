import React, { useState, useEffect } from 'react';

import { Card } from 'semantic-ui-react';

import UsersCard from './UsersCard';

const Users = ({ results, className, onAction }) => {
  const [receivedResults, setReceivedResults] = useState([]);

  useEffect(() => {
    if (results !== undefined) {
      setReceivedResults(results);
    }
  }, [results]);

  return (
    <Card.Group className={className} itemsPerRow={4} doubling>
      {receivedResults.map((result) => (
        <UsersCard key={result.nombre} userData={result} onAction={onAction} />
      ))}
    </Card.Group>
  );
};

export default Users;

