import React, { useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';

import UsersModal from './UsersModal';
import UsersGrid from './UsersGrid';
import { getUsers } from '../../helpers/apiRequest';

import './Users.scss';

const Users = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers()
      .then((data) => setUsers(data))
      .catch((error) => console.error(error.message));
  }, []);

  const addUserTrigger = (
    <Button inverted fluid>
      Agregar Usuario
    </Button>
  );

  return (
    <>
      <UsersModal trigger={addUserTrigger} onClick={setUsers} />
      <UsersGrid className="results" results={users} onAction={setUsers} />
    </>
  );
};

export default Users;
