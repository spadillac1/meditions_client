import React, { useState } from 'react';
import { Button, Form, Header, Modal, Icon } from 'semantic-ui-react';

import { useForm } from '../../hooks/useForm';
import { userForm } from '../../helpers/models';
/* import { */
// getUsers,
// postUsers,
// putUsers,
/* } from '../../helpers/api'; */

const RegistryModal = ({ data, trigger, onClick }) => {
  const [formState, handleInputChange] = useForm(data || userForm);
  const [open, setOpen] = useState(false);

  const { nombre, telefono, apellido } = formState;

  const handleSubmit = () => {
    setOpen(false);
    // if (!data) {
    /*       postUsers(formState) */
    // .then(() => {
    // getUsers(onClick)
    // .then((users) => {
    // alert('User was created');
    // onClick(users);
    // setOpen(false);
    // })
    // .catch(console.error);
    // })
    /* .catch(console.error); */
    // } else {
    /*       putUsers(formState._id, formState) */
    // .then(() => {
    // getUsers(onClick)
    // .then((users) => {
    // alert('User was updated');
    // onClick(users);
    // setOpen(false);
    // })
    // .catch(console.error);
    // setOpen(false);
    // })
    // .catch(console.error);
    /* } */
  };

  return (
    <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size="small"
      trigger={trigger}
    >
      <Header icon>
        <Icon name="user" />
        Agregar Usuario
      </Header>
      <Modal.Content>
        <Form onSubmit={handleSubmit}>
          <Form.Group widths="equal">
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Nombre"
              name="nombre"
              placeholder="nombre"
              required
              value={nombre}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Apellido"
              name="Apellido"
              placeholder="apellido"
              required
              value={apellido}
            />

            <Form.Input
              onChange={handleInputChange}
              fluid
              type="number"
              label="Telefono"
              name="telefono"
              placeholder="telefono"
              required
              value={telefono}
            />
          </Form.Group>
        </Form>
      </Modal.Content>

      <Modal.Actions>
        <Button color="red" inverted onClick={() => setOpen(false)}>
          <Icon name="remove" /> Cancel
        </Button>
        <Button color="green" inverted onClick={handleSubmit}>
          <Icon name="checkmark" /> {data ? 'Update' : 'Registry'}
        </Button>
      </Modal.Actions>
    </Modal>
  );
};
export default RegistryModal;
